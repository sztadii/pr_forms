import axios from 'axios'

const api = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com'
})

api.routes = {
  users: 'users'
}

export default api
