const USERNAME_MAX = 15

const validate = values => {
  const errors = {}

  if (!values.username) {
    errors.username = 'Required'
  } else if (values.username.length > USERNAME_MAX) {
    errors.username = `Must be ${USERNAME_MAX} characters or less`
  }

  if (!values.firstName) {
    errors.firstName = 'Required'
  }

  if (!values.lastName) {
    errors.lastName = 'Required'
  }

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  return errors
}

export default validate
