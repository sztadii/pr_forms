const warn = values => {
  const warnings = {}

  if (values.lastName && values.lastName.length > 20) {
    warnings.lastName = 'Hmm very long last name'
  }

  return warnings
}

export default warn
