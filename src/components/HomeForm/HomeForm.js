import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormSection } from 'redux-form'
import { Button, Notification } from 'element-react'
import InputsCollapse from '../InputsCollapse/InputsCollapse'
import connector from './HomeFormConnector'

export class HomeForm extends Component {
  formSubmit = data => {
    Notification({
      title: 'Success',
      message: JSON.stringify(data, null, 4),
      type: 'success'
    })
    this.props.reset()
  }

  render () {
    const { handleSubmit, pristine, reset, submitting } = this.props

    return (
      <form onSubmit={handleSubmit(this.formSubmit)}>
        <div className='mt-1'>
          <FormSection name=''>
            <InputsCollapse
              names={['username', 'firstName', 'lastName']}
              title='Main'
            />
          </FormSection>
        </div>

        <div className='mt-1'>
          <FormSection name=''>
            <InputsCollapse names={['email']} title='Extra' />
          </FormSection>
        </div>

        <div className='mt-1'>
          <Button disabled={submitting} type='primary' nativeType='submit'>
            Submit
          </Button>
          <Button
            disabled={pristine || submitting}
            onClick={reset}
            type='danger'
            nativeType='submit'
          >
            Clear Values
          </Button>
        </div>
      </form>
    )
  }
}

/* eslint-disable react/boolean-prop-naming */
HomeForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  reset: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired
}

export default connector(HomeForm)
