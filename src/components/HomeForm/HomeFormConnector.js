import { reduxForm } from 'redux-form'
import compose from 'lodash/flowRight'
import validate from './validate'
import warn from './warn'

export default compose(
  reduxForm({
    form: 'homeForm',
    validate,
    warn
  })
)
