import React from 'react'
import PropTypes from 'prop-types'

const Label = ({ type, children }) => (
  <div className={`Label Label--${type}`}>{children}</div>
)

Label.propTypes = {
  type: PropTypes.oneOf(['warning', 'error']).isRequired,
  children: PropTypes.node.isRequired
}

export default Label
