import React from 'react'
import PropTypes from 'prop-types'
import { Input as ElInput } from 'element-react'
import Label from '../Label/Label'

const Input = ({ input, label, type, meta: { touched, error, warning } }) => (
  <div className='Input'>
    <ElInput
      {...input}
      type={type}
      placeholder={label}
      className={touched && error && 'error'}
    />
    {touched &&
      ((error && <Label type='error'>{error}</Label>) ||
        (warning && <Label type='warning'>{warning}</Label>))}
  </div>
)

Input.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'number', 'password']).isRequired,
  meta: PropTypes.object.isRequired
}

export default Input
