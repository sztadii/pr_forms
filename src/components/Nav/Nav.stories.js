import React from 'react'
import { storiesOf } from '@storybook/react'
import { BrowserRouter } from 'react-router-dom'
import Nav from './Nav'

storiesOf('Nav', module).add('in normal mode', () => {
  return (
    <BrowserRouter>
      <Nav />
    </BrowserRouter>
  )
})
