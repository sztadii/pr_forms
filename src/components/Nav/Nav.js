import React from 'react'
import { Link } from 'react-router-dom'
import { Menu } from 'element-react'

const Nav = () => (
  <Menu theme='dark' defaultActive='1' mode='horizontal'>
    <Menu.Item index='1'>
      <Link to='/'>Home</Link>
    </Menu.Item>
    <Menu.Item index='2'>
      <Link to='/info'>Info</Link>
    </Menu.Item>
    <Menu.Item index='3'>
      <Link to='/analytic'>Analytic</Link>
    </Menu.Item>
  </Menu>
)

export default Nav
