import { connect } from 'react-redux'
import compose from 'lodash/flowRight'
import {
  analyticLoading,
  analyticList,
  analyticError
} from 'src/store/main/selectors'
import { AnalyticGetAction } from 'src/store/actions/AnalyticAction'

const mapStateToProps = state => ({
  isLoading: analyticLoading(state),
  error: analyticError(state),
  list: analyticList(state)
})

const mapDispatchToProps = {
  AnalyticGetAction
}

export default compose(connect(mapStateToProps, mapDispatchToProps))
