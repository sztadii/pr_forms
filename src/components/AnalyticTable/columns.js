const columns = [
  {
    label: 'Id',
    prop: 'id',
    width: 30
  },
  {
    label: 'Name',
    prop: 'name'
  },
  {
    label: 'Username',
    prop: 'username'
  },
  {
    label: 'Email',
    prop: 'email'
  }
]

export default columns
