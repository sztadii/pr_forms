import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Alert, Loading, Pagination } from 'element-react'
import columns from './columns'
import connector from './AnalyticTableConnector'

export class AnalyticTable extends Component {
  state = {
    columns
  }

  componentDidMount () {
    this.props.AnalyticGetAction()
  }

  renderLoading = () => <Loading />

  renderError = error => (
    <Alert
      title={error}
      closable={false}
      showIcon
      type='error'
      className='mt-1'
    />
  )

  renderTable = (list, columns) => (
    <div>
      <Table columns={columns} data={list} />
      <Pagination
        layout='prev, pager, next'
        total={20}
        className='text-center mt-1'
      />
    </div>
  )

  render () {
    const { isLoading, error, list } = this.props
    const { columns } = this.state

    if (isLoading) return this.renderLoading()
    if (error) return this.renderError(error)
    return this.renderTable(list, columns)
  }
}

AnalyticTable.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  AnalyticGetAction: PropTypes.func.isRequired
}

export default connector(AnalyticTable)
