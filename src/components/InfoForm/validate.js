const USERNAME_MAX = 15
const PASSWORD_MIN = 5
const QUESTION_MIN = 5

const validate = values => {
  const errors = {}

  if (!values.username) {
    errors.username = 'Required'
  } else if (values.username.length > USERNAME_MAX) {
    errors.username = `Must be ${USERNAME_MAX} characters or less`
  }

  if (!values.firstName) {
    errors.firstName = 'Required'
  }

  if (!values.lastName) {
    errors.lastName = 'Required'
  }

  if (!values.password) {
    errors.password = 'Required'
  } else if (values.password.length < PASSWORD_MIN) {
    errors.password = `Must be ${PASSWORD_MIN} characters or less`
  }

  if (!values.question) {
    errors.question = 'Required'
  } else if (values.question.length < QUESTION_MIN) {
    errors.question = `Must be ${QUESTION_MIN} characters or less`
  }

  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  return errors
}

export default validate
