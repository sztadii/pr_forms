const LASTNAME_WARN = 20

const warn = values => {
  const warnings = {}

  if (values.lastName && values.lastName.length > LASTNAME_WARN) {
    warnings.lastName = 'Hmm very long last name'
  }

  return warnings
}

export default warn
