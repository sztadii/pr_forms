import React from 'react'
import PropTypes from 'prop-types'
import { Collapse } from 'element-react'
import { Field } from 'redux-form'
import startCase from 'lodash/startCase'
import Input from '../Input/Input'

const InputsCollapse = ({ names, title }) => (
  <Collapse value={title}>
    <Collapse.Item title={title} name={title}>
      {names &&
        names.map(name => {
          return (
            <div className='mt-1' key={name}>
              <Field
                component={Input}
                name={name}
                label={startCase(name)}
                type='text'
              />
            </div>
          )
        })}
    </Collapse.Item>
  </Collapse>
)

InputsCollapse.propTypes = {
  names: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
}

export default InputsCollapse
