import React from 'react'
import HomeForm from '../../components/HomeForm/HomeForm'

const Home = () => (
  <div className='mt-1'>
    <div className='container'>
      <HomeForm />
    </div>
  </div>
)

export default Home
