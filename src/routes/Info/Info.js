import React from 'react'
import InfoForm from '../../components/InfoForm/InfoForm'

const Info = () => (
  <div className='mt-1'>
    <div className='container'>
      <InfoForm />
    </div>
  </div>
)

export default Info
