import React from 'react'
import { cleanup, wait } from 'react-testing-library'
import renderWithStore from '../../utils/renderWithStore'
import { nockUsers } from '../../api/api.nock'
import Analytic from './Analytic'

afterEach(cleanup)

test('Analytic displays information fetched from backend', async () => {
  nockUsers()

  const { getByText } = renderWithStore(<Analytic />)
  await wait(() => {
    getByText('Leanne Graham')
  })
})
