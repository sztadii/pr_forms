import React from 'react'
import AnalyticTable from '../../components/AnalyticTable/AnalyticTable'

const Analytic = () => (
  <div className='mt-1'>
    <div className='container'>
      <AnalyticTable />
    </div>
  </div>
)

export default Analytic
