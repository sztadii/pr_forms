import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store/main/initStore'
import './styles/index.scss'
import App from './App'

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <Switch>
        <Route path='/' component={App} />
      </Switch>
    </Provider>
  </BrowserRouter>,
  document.querySelector('#root')
)
