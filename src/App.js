import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'
import loadable from 'loadable-components'
import Nav from './components/Nav/Nav'
import routes from './config/routes'

const Home = loadable(() => import('./routes/Home/Home'))
const Info = loadable(() => import('./routes/Info/Info'))
const Analytic = loadable(() => import('./routes/Analytic/Analytic'))

const App = ({ location }) => (
  <div>
    <Nav />
    <Route
      location={location}
      key={location.key}
      component={({ match }) => (
        <div>
          <Route exact path={`${match.path}`} component={Home} />
          <Route exact path={`${match.path}${routes.info}`} component={Info} />
          <Route
            exact
            path={`${match.path}${routes.analytic}`}
            component={Analytic}
          />
        </div>
      )}
    />
  </div>
)

App.propTypes = {
  location: PropTypes.object.isRequired
}

export default App
