export const ANALYTIC_ERROR = 'ANALYTIC_ERROR'
export const AnalyticErrorAction = error => ({
  type: ANALYTIC_ERROR,
  error
})

export const ANALYTIC_GET = 'ANALYTIC_GET'
export const AnalyticGetAction = () => ({
  type: ANALYTIC_GET
})

export const ANALYTIC_LOADING = 'ANALYTIC_LOADING'
export const AnalyticGetLoadingAction = () => ({
  type: ANALYTIC_LOADING
})

export const ANALYTIC_SUCCESS = 'ANALYTIC_SUCCESS'
export const AnalyticGetSuccessAction = (list = []) => ({
  type: ANALYTIC_SUCCESS,
  list
})
