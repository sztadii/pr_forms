import * as selectors from './selectors'
import { initState } from './initState'

test('analyticLoading returns value of history loading', () => {
  expect(selectors.analyticLoading(initState)).toEqual(false)
})

test('analyticList returns value of history list', () => {
  expect(selectors.analyticList(initState)).toEqual([])
})

test('analyticError returns value of history error', () => {
  expect(selectors.analyticError(initState)).toEqual('')
})
