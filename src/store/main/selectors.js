export const analyticLoading = state => state.AnalyticReducer.loading
export const analyticList = state => state.AnalyticReducer.list
export const analyticError = state => state.AnalyticReducer.error
