import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { AnalyticReducer } from './AnalyticReducer'

export default combineReducers({
  form: formReducer,
  AnalyticReducer
})
