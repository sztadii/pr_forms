import * as todo from '../actions/AnalyticAction'
import { initState } from '../main/initState'

const init = initState.AnalyticReducer

export function AnalyticReducer (state = init, action) {
  switch (action.type) {
    case todo.ANALYTIC_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error
      }
    }

    case todo.ANALYTIC_LOADING: {
      return {
        ...state,
        loading: true,
        error: ''
      }
    }

    case todo.ANALYTIC_SUCCESS: {
      return {
        ...state,
        list: action.list,
        loading: false
      }
    }
  }

  return state
}
