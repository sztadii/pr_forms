import * as analytic from '../actions/AnalyticAction'
import { AnalyticReducer } from './AnalyticReducer'

describe('AnalyticReducer', () => {
  it('returns default state on init', () => {
    const expectState = {
      loading: false,
      list: [],
      error: ''
    }
    expect(AnalyticReducer(undefined, {})).toEqual(expectState)
  })

  it('returns error state after trigger AnalyticErrorAction', () => {
    const expectState = {
      loading: false,
      list: [],
      error: 'Error message'
    }
    expect(
      AnalyticReducer(undefined, analytic.AnalyticErrorAction('Error message'))
    ).toEqual(expectState)
  })

  it('returns loading state after trigger AnalyticGetLoadingAction', () => {
    const expectState = {
      loading: true,
      list: [],
      error: ''
    }
    expect(
      AnalyticReducer(undefined, analytic.AnalyticGetLoadingAction())
    ).toEqual(expectState)
  })

  it('returns success state after trigger AnalyticGetSuccessAction', () => {
    const list = []

    const expectState = {
      loading: false,
      list: list,
      error: ''
    }
    expect(
      AnalyticReducer(undefined, analytic.AnalyticGetSuccessAction(list))
    ).toEqual(expectState)
  })
})
