import { fork } from 'redux-saga/effects'
import { watchAnalytic } from './watchers/watchAnalytic'

export default function * () {
  yield [fork(watchAnalytic)]
}
