import { takeLatest } from 'redux-saga/effects'
import * as actions from '../../actions/AnalyticAction'
import * as triggers from '../workers/workerAnalytic'

export function * watchAnalytic () {
  yield takeLatest(actions.ANALYTIC_GET, triggers.getAnalytic)
}
