import { put, call } from 'redux-saga/effects'
import * as actions from '../../actions/AnalyticAction'
import api from '../../../api/api'

export function * getAnalytic () {
  yield put(actions.AnalyticGetLoadingAction())

  try {
    const { data: analytic } = yield call(api.get, api.routes.users)
    yield put(actions.AnalyticGetSuccessAction(analytic))
  } catch (e) {
    yield put(actions.AnalyticErrorAction(e.message))
  }
}
