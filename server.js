const path = require('path')
const fallback = require('express-history-api-fallback')
const express = require('express')
const compression = require('compression')

const app = express()
const root = path.join(__dirname, '/public')
const port = 7000

app.use(compression({ level: 9 }))
app.use(express.static(root))
app.use(fallback('index.html', { root }))
app.listen(port)

console.log(`App is run on localhost:${port}`)
