import AnalyticPage from '../pages/AnalyticPage'
import routes from '../../src/config/routes'

fixture`Analytic page`.page`${routes.app}${routes.analytic}`

const analytic = new AnalyticPage()

test('Analytic table render correct', async t => {
  await analytic.checkTable(t)
})
