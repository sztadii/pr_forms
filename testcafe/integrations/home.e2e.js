import HomePage from '../pages/HomePage'
import routes from '../../src/config/routes'

fixture`Home page`.page`${routes.app}${routes.home}`

const home = new HomePage()

test('Home form handle error correct', async t => {
  await home.triggerFormError(t)
})

test('Home form handle success correct', async t => {
  await home.triggerFormSuccess(t)
})
