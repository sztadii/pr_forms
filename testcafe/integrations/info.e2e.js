import InfoPage from '../pages/InfoPage'
import routes from '../../src/config/routes'

fixture`Info page`.page`${routes.app}${routes.info}`

const info = new InfoPage()

test('Info form handle error correct', async t => {
  await info.triggerFormError(t)
})

test('Info form handle success correct', async t => {
  await info.triggerFormSuccess(t)
})
