import { Selector } from 'testcafe'

export default class InfoPage {
  constructor () {
    this.usernameInput = Selector('.Input input[name=\'username\']')
    this.firstNameInput = Selector('.Input input[name=\'firstName\']')
    this.lastNameInput = Selector('.Input input[name=\'lastName\']')
    this.emailInput = Selector('.Input input[name=\'email\']')
    this.passwordInput = Selector('.Input input[name=\'password\']')
    this.questionInput = Selector('.Input input[name=\'question\']')
    this.label = Selector('.Input .Label')
  }

  async fillFirstPartForm (t) {
    const username = 'Username'
    const firstName = 'First name'

    await t
      .typeText(this.usernameInput, username)
      .expect(this.usernameInput.value)
      .contains(username)
      .typeText(this.firstNameInput, firstName)
      .expect(this.firstNameInput.value)
      .contains(firstName)
  }

  async fillSecondPartForm (t) {
    const lastName = 'Last name'
    const email = 'mail@mail.com'
    const password = 'Some password'
    const question = 'Some question'

    await t
      .typeText(this.lastNameInput, lastName)
      .expect(this.lastNameInput.value)
      .contains(lastName)
      .typeText(this.emailInput, email)
      .expect(this.emailInput.value)
      .contains(email)
      .typeText(this.passwordInput, password)
      .expect(this.passwordInput.value)
      .contains(password)
      .typeText(this.questionInput, question)
      .expect(this.questionInput.value)
      .contains(question)
  }

  async triggerFormError (t) {
    await this.fillFirstPartForm(t)
    await t
      .pressKey('enter')
      .expect(this.label.count)
      .gt(1)
  }

  async triggerFormSuccess (t) {
    await this.fillFirstPartForm(t)
    await this.fillSecondPartForm(t)
    await t
      .expect(this.label.count)
      .eql(0)
      .expect(this.usernameInput.value)
      .contains('')
      .expect(this.firstNameInput.value)
      .contains('')
      .expect(this.lastNameInput.value)
      .contains('')
      .expect(this.emailInput.value)
      .contains('')
  }
}
