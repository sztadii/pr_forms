import { Selector } from 'testcafe'

export default class HomePage {
  constructor () {
    this.usernameInput = Selector('.Input input[name=\'username\']')
    this.firstNameInput = Selector('.Input input[name=\'firstName\']')
    this.lastNameInput = Selector('.Input input[name=\'lastName\']')
    this.emailInput = Selector('.Input input[name=\'email\']')
    this.label = Selector('.Input .Label')
  }

  async fillFirstPartForm (t) {
    const username = 'Username'
    const firstName = 'First name'

    await t
      .typeText(this.usernameInput, username)
      .expect(this.usernameInput.value)
      .contains(username)
      .typeText(this.firstNameInput, firstName)
      .expect(this.firstNameInput.value)
      .contains(firstName)
  }

  async fillSecondPartForm (t) {
    const lastName = 'Last name'
    const email = 'mail@mail.com'

    await t
      .typeText(this.lastNameInput, lastName)
      .expect(this.lastNameInput.value)
      .contains(lastName)
      .typeText(this.emailInput, email)
      .expect(this.emailInput.value)
      .contains(email)
  }

  async triggerFormError (t) {
    await this.fillFirstPartForm(t)
    await t
      .pressKey('enter')
      .expect(this.label.count)
      .gt(1)
  }

  async triggerFormSuccess (t) {
    await this.fillFirstPartForm(t)
    await this.fillSecondPartForm(t)
    await t
      .expect(this.label.count)
      .eql(0)
      .expect(this.usernameInput.value)
      .contains('')
      .expect(this.firstNameInput.value)
      .contains('')
      .expect(this.lastNameInput.value)
      .contains('')
      .expect(this.emailInput.value)
      .contains('')
  }
}
