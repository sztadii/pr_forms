import { Selector } from 'testcafe'

export default class AnalyticPage {
  constructor () {
    this.tableRow = Selector('.el-table__body tr')
  }

  async checkTable (t) {
    await t
      .click(this.tableRow)
      .expect(this.tableRow.count)
      .gt(1)
  }
}
